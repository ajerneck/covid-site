#!/bin/bash

docker run -it \
       --mount type=bind,source=/home/alexander/start/play/r/covid-site,target=/app \
       --env CENSUS_KEY=$(cat census_key.cred) \
       --workdir /app \
       sir_env \
       sh generate.sh
