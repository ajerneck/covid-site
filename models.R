##' ---
##' title: "SIR Models"
##' author: ""
##' date: "Last updated: `r Sys.time()`"
##' output:
##'     html_document:
##'         fig_width: 24
##'         fig_height: 24
##'         number_sections: true
##'         code_folding: hide
##' ---

## make the html content cover the entire page.
## https://stackoverflow.com/a/46564977/1072349
##+ css
##' <style type="text/css">
##'            .main-container {
##'                max-width: 1800px;
##'                margin-left: auto;
##'                margin-right: auto;
##'            }
##' </style>
##'

##+ chunk_options, message=FALSE
knitr::opts_chunk$set(
                      message=FALSE
                    , warning=FALSE
                  )

##' # Libraries and setup
##'
library(dplyr)
library(ggplot2)
library(purrr)
library(readr)
library(rstan)
library(stringr)
library(tidyr)



rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())

source("stan_utility.R")
source("functions.R")

theme_report <- plot_theme()

## TODO: can probably delete?
params <- list(beta = beta,
               gamma = gamma)

##' # SIR model for PA

## Currently the model is only fit to PA data.

##' ## Load data

ds <- load_data()

df <- ds$df %>% filter(state %in% c("PA", "NY", "MA"))

df <- df %>%
    select(-fips, -dataQualityGrade) %>%
    filter(date > as.Date("2020-04-01")) %>%
    filter(name %in% c("positiveIncrease", "totalTestResultsIncrease")) %>%
    drop_na() %>%
    ## avoid zeros
    filter(value > 0) %>%
    arrange(state, name, date) %>%
    group_by(state, name) %>%
    mutate(t = seq_along(date)) %>%
    ungroup %>%
    pivot_wider(names_from = "name", values_from = "value")

df %>%
    mutate(percentPositive = positiveIncrease / totalTestResultsIncrease) %>%
    ggplot() +
    geom_point(aes(t, percentPositive, color = state, size = totalTestResultsIncrease)) +
    geom_line(aes(t, percentPositive, color = state)) +
    theme_report

##' ## Set up data structure for stan

## ## sample_n is the max number of tests per day.
## sample_n <- df %>%
##     top_n(1, totalTestResultsIncrease) %>%
##     pull(totalTestResultsIncrease)


## this is to use the number of tests
## sample_n <- df$totalTestResultsIncrease
## this is to use population
sample_n <- df %>%
    left_join(ds$pop_data %>%
              rename(state = abb)
              ) %>%
    group_by(state) %>%
    summarise(pop = round(max(pop) * 1e-3))

df <- df %>%
    mutate(state_id = as.numeric(factor(state)))



state_mapping <- df %>%  distinct(state, state_id)

md <- df %>%
    left_join(sample_n) %>%
    group_by(state_id) %>%
    mutate(sample_days = n_distinct(date)) %>%
    select(sample_days, sample_y=positiveIncrease, sample_n = pop, sample_time = t)


md %>% mutate(prob = sample_y/sample_n) %>% arrange(prob)

t_max <- max(md$sample_time) + 30
t_max

md %>%
    mutate(percentPositive = sample_y / sample_n) %>%
    pivot_longer(c(sample_y, percentPositive)) %>%
    ggplot() +
    geom_line(aes(sample_time, value, color = factor(state_id))) +
    facet_wrap(~name, scales = "free", ncol = 1) +
    theme_report

##' # Fitting

md <- md %>% arrange(state_id, sample_time) %>% ungroup

## calculate how many states
n_states <- md %>% summarise(n_distinct(state_id)) %>% pull %>% max

## calculate how many obs the smallest state has, limit all states to that.
n_obs <- min(md$sample_days)
md <- md %>% filter(sample_time <= n_obs)


stan_d_m <- list(
    n_states = n_states
  , n_sample = md %>% group_by(state_id) %>% summarise(max(sample_n)) %>% pull
  , n_params = 2
  , n_difeq = 3
  , n_obs = n_obs
  , n_fake = length(min(md$sample_time):t_max)
  , y = md %>% ungroup %>% select(sample_y) %>% pull() %>%  matrix(., ncol = n_states)
  , t0 = 0
  , ts = md %>% distinct(sample_time) %>% pull()
  , fake_ts = c(min(md$sample_time):t_max)
)

str(stan_d_m)


# Which parameters to monitor in the model:
params_monitor = c("y_hat", "y0", "params", "fake_I", "S0", "fake_y")

# Test / debug the model:
test = stan("SI_h_fit.stan",
            data = stan_d_m,
            pars = params_monitor,
            chains = 1
          , iter = 10
            ## , init = list(mk_inits())
          ## , init = list(mk_inits())
            , seed = 1234
            )

# Fit and sample from the posterior
mod = stan(fit = test,
           data = stan_d_m,
           pars = params_monitor,
           chains = 1,
           warmup = 300,
           iter = 600
           , verbose = TRUE
           , seed = 1234
           ## , init = purrr::map(1:3, ~list(y_hat = 0))
           )

##' ## Model summary
##'

# Extract the posterior samples to a structured list:
posts <- rstan::extract(mod)


##' ## Initial proportion of

round(apply(posts$y0, 2, median), 2)

##' #  Predictions from the model

qs <- list(low = 0.025, median = 0.5, high = 0.975)
qs <- purrr::map(qs, ~ apply(posts$fake_y, c(2,3), quantile, probs = c(.)))
qs <- purrr::map_df(qs, ~ as_tibble(.) %>% dplyr::rename_all(readr::parse_number), .id = "q") %>%
    pivot_longer(-q, names_to = "state_id")
qs <- qs %>% group_by(state_id, q) %>%
    mutate(sample_time = 1:n()) %>%
    pivot_wider(names_from = "q", values_from = "value") %>%
    ungroup %>%
    mutate(state_id = as.numeric(state_id))

## TODO: translate sample_time to actual dates.
left_join(qs, md) %>%
    inner_join(state_mapping) %>%
    mutate(date = min(df$date) + sample_time) %>%
    ggplot() +
    geom_line(aes(date, median, color = state)) +
    geom_ribbon(aes(date, ymin = low, ymax = high, fill = state), alpha = 0.3) +
    geom_line(aes(date, sample_y, color = state)) +
    facet_wrap(~state, scales = "free_y", ncol = 1) +
    theme_report


## # Combine into two data frames for plotting
## df_sample = tibble(sample_prop, sample_time)
## df_fit = tibble(mod_median, mod_low, mod_high, sample_time = mod_time) %>%
##     mutate_at(vars(mod_median, mod_low, mod_high), funs(. / sample_n))



##' ## Beta, gamma and R0

## \beta is the infection rate, and \gamma is the recovery rate.

ptmp <- posts$params %>%
    as_tibble %>%
    mutate(iter = 1:n()) %>%
    pivot_longer(-iter) %>%
    extract("name", into = c("variable", "state_id"), regex = "([0-9]+)\\.([0-9]+)") %>%
    mutate(state_id = as.numeric(state_id)) %>%
    inner_join(state_mapping) %>%
    mutate(variable = recode(variable, `1` = "beta", `2` = "gamma"))

## ptmp %>%
##     ggplot() +
##     geom_histogram(aes(value, fill = state )) +
##     facet_wrap(state~variable, scales = "free") 

p1 <- ptmp %>%
    pivot_wider(names_from = "variable") %>%
    mutate(R0 = beta / gamma)


quants <- function(x) {
    quantile(x, probs = c(0.025, 0.5, 0.975)) %>%
        t() %>%
        as_tibble() %>%
        set_names(c("ymin", "y", "ymax"))
}

p1 %>%
    ggplot(aes(state, R0)) +
    stat_summary(fun.data = quants) +
    geom_hline(aes(yintercept = 1)) +
    theme_report

##' Estimated R0 for each state.
p1 %>%
    group_by(state) %>%
    do(quants(.$R0)) %>%
    knitr::kable(digits = 2)

ptmp %>%
    ggplot() +
    geom_histogram(aes(value)) +
    facet_wrap(state ~ variable, scales = "free") +
    theme_report


## ## -- end of current massage of params.

## ptmp <- apply(posts$params, 2, quantile, probs=c(0.025, 0.5, 0.975)) %>%
##     t %>%
##     as_tibble %>%
##     mutate(variable = c("beta", "gamma"))

## ptmp %>%
##     ggplot() +
##     geom_pointrange(aes(variable, y=`50%`, ymin = `2.5%`, ymax = `97.5%`)) +
##     ylab("Estimate") +
##     theme_report

## ptmp <- posts$params %>%
##     as_tibble() %>%
##     rename(beta = V1, gamma = V2) %>%
##     mutate(r0 = beta/gamma) %>%
##     pivot_longer(c(beta, gamma, r0))

## ptmp %>%
##     ggplot() +
##     geom_histogram(aes(value)) +
##     facet_wrap(~name, scales = "free") +
##     geom_vline(aes(xintercept = x), data = ptmp %>% group_by(name) %>% summarise(x = median(value))) +
##     theme_report



##'


##' Susceptible, Infected and Recovered.
##'
##'

## indices <- list(susceptible = 1, infected = 2, recovered = 3)

## pms <- map_df(indices, function(i) {
##     posts$y_hat[,,i] %>%
##         t %>%
##         as_tibble %>%
##         mutate(date = seq(min(df$date), by = 1, length.out = n())) %>%
##         pivot_longer(-date) %>%
##         rename(iter = name) %>%
##         mutate(iter = readr::parse_number(iter))
## }, .id = "compartment")

## iters_to_sample <- tibble(iter = sample(1:nrow(posts$fake_y), 100) )

## pms %>%
##     inner_join(iters_to_sample) %>%
##     ggplot() +
##     geom_line(aes(date, value, group = interaction(iter, compartment), color = compartment), alpha = 0.03) +
##     theme_report



## ##' ## Model Prediction
## ptmp <- left_join(df_fit, df_sample) %>%
##     mutate(date = seq(min(df$date), length.out = n(), by = 1))

## ptmp %>%
##     ggplot() +
##     geom_line(aes(date, sample_prop, color = "actual")) +
##     geom_line(aes(date, mod_median, color = "pred_median")) +
##     geom_ribbon(aes(date, ymin = mod_low, ymax = mod_high), fill = "blue", alpha = 0.3) +
##     xlab("Time") +
##     geom_line(aes(date, value, group = interaction(iter, compartment), color = compartment), alpha = 0.03, data = inner_join(pms, iters_to_sample)) +
##     theme_report



## ##' Note: sample_prop is positive cases per 1000 capita.


## ptmp_y <- posts$fake_y %>%
##     as_tibble %>%
##     mutate(iter = 1:n()) %>%
##     pivot_longer(-iter) %>%
##     inner_join(iters_to_sample) %>%
##     mutate(
##         sample_time = readr::parse_number(name)
##         , value = value / sample_n
##     )

## ptmp_y %>%
##     ggplot() +
##     geom_line(aes(sample_time, value, group = iter), alpha = 0.03) +
##     theme_report

## ## ptmp_y <- posts$fake_y_deaths %>%
## ##     as_tibble %>%
## ##     mutate(iter = 1:n()) %>%
## ##     pivot_longer(-iter) %>%
## ##     inner_join(iters_to_sample) %>%
## ##     mutate(
## ##         sample_time = readr::parse_number(name)
## ##       , value = value
## ##     )

## ## ptmp_y %>%
## ##     ggplot() +
## ##     geom_line(aes(sample_time, value, group = iter), alpha = 0.03)



##' # Diagnostics
##' following
##' https://betanalpha.github.io/assets/case_studies/rstan_workflow.html
check_rhat(mod)
check_treedepth(mod)
check_energy(mod)
check_div(mod)

traceplot(mod, pars=c("S0"))
traceplot(mod, pars=c("params"))
traceplot(mod, pars=c("y0"))


