# Tracking Covid

This site has some plots and analysis of COVID-19 in the United States.

- The generated report is available at https://ajerneck.gitlab.io/covid-site/report.html

- Data on COVID-19 cases are downloaded from [The COVID Tracking Project](https://covidtracking.com/data/download).

- The SIR model draws on this [blog post](https://jrmihalj.github.io/estimating-transmission-by-fitting-mechanistic-models-in-Stan/).

- Some Stan diagnostics are taken from [here](https://github.com/betanalpha/knitr_case_studies/blob/d8eb7466b16bd345030a186af1f5c1a849a8595d/principled_bayesian_workflow/stan_utility.R~).

