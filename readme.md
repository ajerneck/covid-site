# Doing 
   
# Todo



* use gitlab pipeline to 
*    test (just that the script generates), and only deploy if it passes.

* clean up: make a nice home page
* clean up: set font sizes

* incorporate data quality measures.
* add script to test dockerfile locally by running generate.sh in it.
* fix coverage section when run in the cloud, maybe it's due to UTC?


- model enhancements from wikipedia
  - fit SEIR model 
  - fit SIRD model
  - fit variable contact rates

- merge into master

- SIR
  - run one model per state.
  - revise model to be partially pooled across states.

- make header icons consistent across all files.


# Done

* move to gitlab
* use rmarkdown site generator
* use gitlab pipeline to run
* use a custom dockerimage to speed things up
* set up pipeline step for building and pushing docker image.
    - how to build and push: https://gitlab.com/ajerneck/covid-site/container_registry CLI commands button.
* v2: try a time series forecast, to see how things are going for each.

* bring in rest of report
* clean up: remove warnings, echos etc.
* clean up: use the same theme

* v3: fit SIR in stan
  - replicate https://github.com/ajerneck/COMMAND_stan.git
  - adapt model to one state with lots of good data.

* need better summary / at a glance section: these are the states with the greatest increase in the last 7 days.

# Howto

## Build and run the docker image

    # build the docker image
    docker build --tag sir_env .

    # run it
    docker run -it sir_env bash

## Mount a volume in it

    # mount a directory as a (bind) volume
    docker run -it --mount type=bind,source=<ABSOLUTE PATH>,target=/app sir_env ls app/

## Run generate in the docker container

    ./generate_in_docker.sh

