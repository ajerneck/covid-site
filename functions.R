
library(dplyr)
library(maps)
library(stringr)
library(readr)
library(lubridate)
library(tidyr)

load_pop_data_from_census <- function(state_names) {

    library(censusapi)

    census_key <- Sys.getenv("CENSUS_KEY")

    pop_data <- getCensus(
        key = census_key
      , name = "pep/population"
      , vintage = 2019
      , vars = c("POP", "DENSITY")
      , region = "state"
    ) %>%
        as_tibble

    pop_data <- pop_data %>%
        rename(fips = state, pop = POP, density = DENSITY) %>%
        mutate_at(vars(pop, density, fips), funs(as.numeric))

    left_join(state_names, pop_data)

}



load_pop_data <- function() {

    read_csv("state_pop.csv")

}

load_data <- function() {
    state_names <-  state.fips %>%
        distinct(fips, abb, region = str_replace(polyname, ":.*", "")) %>%
        as_tibble

    ##+ read_covid
    df <- read_csv("https://covidtracking.com/api/v1/states/daily.csv")
    df <- df %>%
        mutate(fips = as.numeric(fips))

    pop_data <- load_pop_data()

    ##+ transform
    ## transform and clean
    df <- df %>%
        mutate(date = ymd(as.character(date))) %>%
        select(-hash, -dateChecked)

    df <-df %>%
        select(-lastUpdateEt, -dateModified, -checkTimeEt, -grade, -totalTestResultsSource) %>%
        pivot_longer(c(-date, -state, -fips, -dataQualityGrade))



    list(df = df
       , pop_data = pop_data
       , state_names = state_names
         )
}

plot_theme <- function() {

    theme_minimal(base_size = 23)
}
