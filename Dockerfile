FROM rocker/r-ver:3.6.3

RUN apt-get update -qq

# RUN apt-get -y --no-install-recommends install \
#   libxml2-dev \
#   libcairo2-dev \
#   libsqlite-dev \
#   libmariadbd-dev \
#   libmariadbclient-dev \
#   libpq-dev \
#   libssh2-1-dev \
#   unixodbc-dev \
#   libsasl2-dev

RUN install2.r --error \
  dplyr

RUN install2.r --error \
  ggplot2

RUN install2.r --error \
  lubridate

RUN install2.r --error \
  maps

RUN install2.r --error \
  readr

RUN install2.r --error \
  tidyr

RUN install2.r --error \
  rmarkdown

RUN apt-get -y --no-install-recommends install pandoc

RUN apt-get -y install libcurl4-openssl-dev

RUN install2.r --error \
  forecast

RUN apt-get -y install libssl-dev

RUN install2.r --error \
  censusapi

RUN install2.r --error rstan
