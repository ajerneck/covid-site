// This code originally comes from: https://jrmihalj.github.io/estimating-transmission-by-fitting-mechanistic-models-in-Stan/
// but has been modified.


functions {
  
  // This largely follows the deSolve package, but also includes the x_r and x_i variables.
  // These variables are used in the background.
  
  real[] SI(real t,
            real[] y,
            real[] params,
            real[] x_r,
            int[] x_i) {
      
      real dydt[3];
      
      dydt[1] = - params[1] * y[1] * y[2]; // Susceptible
      dydt[2] = params[1] * y[1] * y[2] - params[2] * y[2]; // Infected
      dydt[3] = params[2] * y[2]; // Recovered
      
      return dydt;
    }
  
}

data {
  int<lower = 1, upper = 55> n_states; // Number of states
  int<lower = 1> n_obs; // Number of days sampled
  int<lower = 1> n_params; // Number of model parameters
  int<lower = 1> n_difeq; // Number of differential equations in the system
  int<lower = 1> n_sample[n_states]; // Number of hosts sampled at each time point.
  int<lower = 1> n_fake; // This is to generate "predicted"/"unsampled" data
  
  int<lower = 0> y[n_obs, n_states]; // The binomially distributed data
  real t0; // Initial time point (zero)
  real ts[n_obs]; // Time points that were sampled
  
  real fake_ts[n_fake]; // Time points for "predicted"/"unsampled" data
}

transformed data {
  real x_r[0];
  int x_i[0];
}

parameters {
  real<lower = 0, upper = 1> params[n_params, n_states]; // Model parameters
  real<lower = 0, upper = 1> S0; // Initial fraction of hosts susceptible
  real<lower = 0, upper = 1> R0; // Initial fraction of hosts recovered
}

transformed parameters{
  real<lower=0, upper=1> y_hat[n_obs, n_difeq, n_states]; // Output from the ODE solver
  real y0[n_difeq]; // Initial conditions for both S and I

  y0[1] = S0;
  y0[2] = (1 - S0)/4;
  y0[3] = (1 - S0)/4;

  for(i in 1:n_states)
        y_hat[,,i] = integrate_ode_bdf(SI, y0, t0, ts, params[,i], x_r, x_i);
  
}

model {
  // priors from scratch
  // these should probably be better!
  S0 ~ beta(10, 1);
  R0 ~ beta(100, 1);
  params[1] ~ beta(5, 11); // beta: nr of infections per period
  params[2] ~ beta(5, 5); // gamma: recovery rate 

  for(i in 1:n_states)
        y[,i]  ~ binomial(n_sample[i], y_hat[, 2, i]); //y_hat[,2] are the fractions infected from the ODE solver
  
}

generated quantities {
  // Generate predicted data over the whole time series:
  real<lower=0, upper=1> fake_I[n_fake, n_difeq, n_states];
  int<lower = 0> fake_y[n_fake, n_states];
  
  
  for(i in 1:n_states) {
        fake_I[,,i] = integrate_ode_bdf(SI, y0, t0, fake_ts, params[,i], x_r, x_i);
        
        fake_y[,i] = binomial_rng(n_sample[i], fake_I[, 2, i]);
  }  
}
 
